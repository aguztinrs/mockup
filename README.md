INSTALLATION
------------
Install and enable this theme in the well-known way.

- HOW IT WORKS?
This theme show all the blocks positioned in its respective region on the website, clearly highlighted the place where the blocks are placed. It's very useful for sitebuilding developments when only use mockups as a reference, so you can build the site defining the escencial regions and the final desing can be in process at the same time.

Regions are expanded by default but you can collpased them clicking the title in each region. This theme has very simple styles because its main functionality is showing blocks and its respecting regions.

LIBRARIES
---------
- This module attached bootstrap library just to use a few behaviours. 

REGIONS
-------
- In this theme we defined some basic regions that we considered necesary for many common structures, we have regions for headers, content area, sidebars and footers.

